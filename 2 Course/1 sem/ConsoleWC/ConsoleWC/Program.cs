﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ConsoleWC
{
    internal static class Program
    {
        public static async Task Main(string[] args)
        {
            // WebRequest
            // используем WebRequest
            await UseWebRequest();
            
            // WebClient
            // скачивание файла с помощью WebClient
            UseWebClient();
            // добавление хэдеров
            UseWebClientWithHeaders();
            // вывод полученных хэдеров ответа
            await UseWebClientPrintHeaders();
            // работа с json
            UseWebClientJSON();
            
            // HTTPClient
            await UseHTTPClientJSON();
            
        }

        private static async Task UseWebRequest()
        {
            var request = WebRequest.CreateHttp("https://reqres.in/api/users/2");
            var response = await request.GetResponseAsync();
            using (var stream = response.GetResponseStream())
            {
                if (stream != null)
                    using (var reader = new StreamReader(stream))
                    {
                        var userJson = await reader.ReadToEndAsync();
                        Console.WriteLine(userJson);
                    }    
            }
            response.Close();
        }

        private static void UseWebClient()
        {
            WebClient client = new WebClient();
            var link = "https://media-www-asp.azureedge.net/media/5245130/home-hero-2.png";
            var bytes = client.DownloadData(link);
            var name = System.IO.Path.GetFileName(link);
            System.IO.File.WriteAllBytes(name, bytes);
        }

        private static void UseWebClientWithHeaders()
        {
            WebClient client = new WebClient();
            client.Encoding = Encoding.UTF8;
            client.QueryString.Add("q", "search text");
            client.Headers.Add("User-Agent", "Mozilla/5.Gecko/20101026 Firefox/3.6.12");
            client.Headers.Add("Accept-Language", "ru");
            var bytes = client.DownloadData("https://www.google.ru/search");
            var page = Encoding.UTF8.GetString(bytes);
            File.WriteAllText("search2.html", page);
        }

        private static async Task UseWebClientPrintHeaders()
        {
            WebClient client = new WebClient();
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://google.com");
            HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
            WebHeaderCollection headers = response.Headers;
            for(int i=0; i < headers.Count; i++)
            {
                Console.WriteLine("{0}: {1}", headers.GetKey(i), headers[i]);
            }
            response.Close();
        }
        
        private static void UseWebClientJSON()
        {
            WebClient client = new WebClient();
            // GET
            client.Encoding = Encoding.UTF8;
            client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
            client.QueryString.Add("page", "2");
            var jsonData = client.DownloadString("https://reqres.in/api/users");
            Console.WriteLine(jsonData);
            Console.WriteLine();
            
            // POST
            client.Headers.Add(HttpRequestHeader.ContentType,"application/json");
            var json = new {email="eve.holt@reqres.in", password="cityslicka"};
            var str = Newtonsoft.Json.JsonConvert.SerializeObject(json);
            var response= client.UploadString("https://reqres.in/api/login",str);
            var tokenObj = JsonConvert.DeserializeAnonymousType(response, new{token=""});
            Console.WriteLine(tokenObj);

        }

        private static async Task UseHTTPClientJSON()
        {
            using (var client = new HttpClient())
            {
                var json = new { email = "eve.holt@reqres.in", password = "cityslicka" };
                var str = Newtonsoft.Json.JsonConvert.SerializeObject(json);

                var content = new StringContent(str);
                content.Headers.ContentType =  new MediaTypeHeaderValue("application/json");
                var result = await client.PostAsync("https://reqres.in/api/login", content);
                
                var response = await result.Content.ReadAsStringAsync();
                var tokenObj = JsonConvert.DeserializeAnonymousType(response, new { token = "" });
                Console.WriteLine(tokenObj);
            }
        }
    }
}